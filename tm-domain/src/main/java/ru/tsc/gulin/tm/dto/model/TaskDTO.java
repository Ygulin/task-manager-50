package ru.tsc.gulin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.model.IWBS;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.listener.EntityListener;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateEnd;

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final String description,
            @NotNull final Date dateStart
    ) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.dateStart = dateStart;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " +
                getStatus().getDisplayName() + " : " + description;
    }

}
