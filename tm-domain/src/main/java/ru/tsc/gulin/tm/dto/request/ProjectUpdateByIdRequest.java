package ru.tsc.gulin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
