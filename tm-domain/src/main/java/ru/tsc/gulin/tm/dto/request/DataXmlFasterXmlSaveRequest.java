package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataXmlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
