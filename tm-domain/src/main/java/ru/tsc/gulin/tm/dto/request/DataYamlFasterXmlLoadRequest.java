package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataYamlFasterXmlLoadRequest extends AbstractUserRequest {

    public DataYamlFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
