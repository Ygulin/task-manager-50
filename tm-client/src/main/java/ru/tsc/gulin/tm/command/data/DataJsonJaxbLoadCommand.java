package ru.tsc.gulin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.request.DataJsonJaxbLoadRequest;
import ru.tsc.gulin.tm.dto.request.UserLogoutRequest;
import ru.tsc.gulin.tm.enumerated.Role;

public final class DataJsonJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD DATA FROM JSON FILE]");
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonJaxbLoadRequest(getToken()));
        serviceLocator.getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
