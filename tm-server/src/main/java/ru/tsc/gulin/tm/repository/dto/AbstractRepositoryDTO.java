package ru.tsc.gulin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.gulin.tm.comparator.DateStartComparator;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.gulin.tm.comparator.CreatedComparator;
import ru.tsc.gulin.tm.comparator.StatusComparator;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;
import ru.volnenko.lib.util.GenericUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    protected Class<M> getEntityClass() {
        return (Class<M>) GenericUtil.getClassFirst(this.getClass());
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateStartComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getEntityClass().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntityClass()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        @NotNull final Root<M> from = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
