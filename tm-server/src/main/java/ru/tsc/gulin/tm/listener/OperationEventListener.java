package ru.tsc.gulin.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.service.ISenderService;
import ru.tsc.gulin.tm.dto.event.OperationEvent;
import ru.tsc.gulin.tm.service.SenderService;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public final class OperationEventListener implements Consumer<OperationEvent> {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final ISenderService service = new SenderService();

    @Override
    @SneakyThrows
    public void accept(@NotNull final OperationEvent operationEvent) {
        @NotNull final Class entityClass = operationEvent.getEntity().getClass();
        if (!entityClass.isAnnotationPresent(Table.class)) return;
        @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
        @NotNull final Table table = (Table) annotation;
        operationEvent.setTable(table.name());
        @NotNull final String json = objectWriter.writeValueAsString(operationEvent);
        executorService.submit(() -> service.send(json));
    }

}
