package ru.tsc.gulin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.UserDTO;
import ru.tsc.gulin.tm.enumerated.Role;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    );

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    Boolean isLoginExists(@NotNull String login);

    Boolean isEmailExists(@NotNull String email);

}
