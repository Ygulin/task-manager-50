package ru.tsc.gulin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
