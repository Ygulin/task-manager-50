package ru.tsc.gulin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    M findOneById(@NotNull String id);

    int getSize();

    void remove(@NotNull M model);

    void update(@NotNull M model);

}
