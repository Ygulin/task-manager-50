package ru.tsc.gulin.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import ru.tsc.gulin.tm.service.ConnectionService;
import ru.tsc.gulin.tm.service.PropertyService;

public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void Test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);
    }

}
